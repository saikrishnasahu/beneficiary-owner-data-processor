package com.bnym.beneficiary.owner.data.processor.helper;

import com.bnym.beneficiary.owner.data.processor.model.Position;
import com.bnym.beneficiary.owner.data.processor.model.Result1Map;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

//@Component
public class RESTHelper {

    static RestTemplate restTemplate = new RestTemplate();
    //    @Value("${position.data.url}")
    private static final String positionDataUrl = "http://localhost:9090/position/all";
    //    @Value("${output.data.url}")
    private static final String outputDataUrl = "http://localhost:9090/position/print";

    public static List<Position> getPositions() throws JsonProcessingException {
        System.out.println(positionDataUrl);
        String response = restTemplate.getForObject(positionDataUrl, String.class);
        return Arrays.asList(new ObjectMapper().readValue(response, Position[].class));
    }

    public static void postResult(Result1Map map) {
        restTemplate.postForObject(outputDataUrl, map, Void.class);
    }
}

